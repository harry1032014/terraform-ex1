resource "aws_instance" "web" {
  ami           = "ami-05295b6e6c790593e"
  instance_type = "t2.micro"

  tags = {
    Name = "HelloWorld-dev"
  }
} 
provider "aws" {
  access_key                  = "test"
  secret_key                  = "test"
  region                      = "us-east-1"
  s3_use_path_style           = false
  skip_credentials_validation = true
  skip_metadata_api_check     = true
  skip_requesting_account_id  = true

  endpoints {
    apigateway     = "http://10.104.143.235:4566"
    apigatewayv2   = "http://10.104.143.235:4566"
    cloudformation = "http://10.104.143.235:4566"
    cloudwatch     = "http://10.104.143.235:4566"
    dynamodb       = "http://10.104.143.235:4566"
    ec2            = "http://10.104.143.235:4566"
    es             = "http://10.104.143.235:4566"
    elasticache    = "http://10.104.143.235:4566"
    firehose       = "http://10.104.143.235:4566"
    iam            = "http://10.104.143.235:4566"
    kinesis        = "http://10.104.143.235:4566"
    lambda         = "http://10.104.143.235:4566"
    rds            = "http://10.104.143.235:4566"
    redshift       = "http://10.104.143.235:4566"
    route53        = "http://10.104.143.235:4566"
    s3             = "http://s3.10.104.143.235.localstack.cloud:4566"
    secretsmanager = "http://10.104.143.235:4566"
    ses            = "http://10.104.143.235:4566"
    sns            = "http://10.104.143.235:4566"
    sqs            = "http://10.104.143.235:4566"
    ssm            = "http://10.104.143.235:4566"
    stepfunctions  = "http://10.104.143.235:4566"
    sts            = "http://10.104.143.235:4566"
  }
}